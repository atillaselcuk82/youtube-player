# Youtube Player

Youtube Player app displaying both single video and a playlist written in Kotlin-Android by following the Udemy course of Tim Buchalka.

# Topics

1. add YouTubeAndroidPlayerApi lib
2. Create a YouTube playerView in code
3. Create google API key (SHA1 -> Gradle -> Tasks -> android -> signingReport
4. Setup Youtube API key from console.developers.google.com (120)
5. Setup override functions onInitializationFaillure and onInitializationSuccess
6. Add callbacks playBackEventListener and playerStateChangeListener
7. OnClickListener interface
8. Intents

# Screens

<img src="screen_images/homescreen.jpeg" alt="drawing" width="175" /> &nbsp;&nbsp;&nbsp; <img src="screen_images/single_video.jpeg" alt="drawing" width="175"/> &nbsp;&nbsp;&nbsp; <img src="screen_images/standalone_screen.jpeg" alt="drawing" width="175"/> &nbsp;&nbsp;&nbsp; <img src="screen_images/playlist.jpeg" alt="drawing" width="175"/>
